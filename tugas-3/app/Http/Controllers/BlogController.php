<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class BlogController extends Controller
{
    public function home(){
        $mahasiswa = DB::table('mahasiswa')->get();
        return view('home',compact('mahasiswa'));
    }

    public function index(){
        return view('index');
    }

    public function create(){

        return view('create');
    }

    public function show($id){
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get();
        return view('show',compact('mahasiswa'));
    }

    //method untuk insert data ke table mahasiswa
    public function store(Request $request){
        //insert data ke table mahasiswa
        DB::table('mahasiswa')->insert([
            'Nama' => $request->Nama,
            'Jurusan' => $request->Jurusan,
            'Kelas' => $request->Kelas,
            'Angkatan' => $request->Angkatan,
            'Email' => $request->Email,
            'Alamat' => $request->Alamat
        ]);

        //mengalihkan halaman ke halaman siswa
        return redirect('/');
    }   

    //method untuk hapus data mahasiswa
    public function hapus($id){
        //menghapus data siswa berdasarkan id yang dipilih
        DB::table('mahasiswa')->where('id',$id)->delete();
        //kembali ke halaman utama
        return redirect('/');
    }

    public function edit($id){
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get();
        return view('edit',compact('mahasiswa'));
    }

    //method untuk update data mahasiswa
    public function update(Request $request){
        DB::table('mahasiswa')->where('id',$request->id)->UPDATE([
            'Nama' => $request->Nama,
            'Jurusan' => $request->Jurusan,
            'Kelas' => $request->Kelas,
            'Angkatan' => $request->Angkatan,
            'Email' => $request->Email,
            'Alamat' => $request->Alamat
        ]);
        //kembali ke halaman utama
        return redirect('/');
    }
}
